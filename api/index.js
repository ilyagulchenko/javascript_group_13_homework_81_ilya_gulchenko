const express = require('express');
const cors = require('cors');
const mongoDb = require('./mongoDb');
const link = require('./app/link');
const app = express();

const port = 8000;

app.use(cors({origin: 'http://localhost:4200'}));
app.use(express.json());
app.use(express.static('public'));
app.use('/link', link);

const run = async () => {
    await mongoDb.connect();

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    process.on('exit', () => {
        mongoDb.disconnect();
    });
};

run().catch(e => console.error(e));
