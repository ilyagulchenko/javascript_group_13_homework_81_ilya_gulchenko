const express = require('express');
const { customAlphabet } = require('nanoid');
const mongoDb = require('../mongoDb');

const router = express.Router();

router.get('/', async (req,res,next) => {
    try {
        const db = mongoDb.getDb();
        const results = await db.collection('link').find().toArray();

        return res.send(results);
    } catch (e) {
        next(e);
    }
});

router.get('/:short', async (req, res, next) => {
    try {
        const db = mongoDb.getDb();
        const result = await db.collection('link').findOne({shortUrl: req.params.short});

        res.status(301).redirect(result.userLink);
    } catch (e) {
        next(e);
    }
})

router.post('/', async (req, res, next) => {
    try {
        const db = mongoDb.getDb();

        const newUrl = {
            userLink: req.body.userLink,
            shortUrl: customAlphabet('1234567890abcdef', 6)(),
        };

        const result = await db.collection('link').insertOne(newUrl);

        return res.send(newUrl);
    } catch (e) {
        next(e);
    }
});

module.exports = router;
