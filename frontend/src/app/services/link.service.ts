import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { LinkData } from '../models/link.model';

@Injectable({
  providedIn: 'root'
})
export class LinkService {

  constructor(private http: HttpClient) { }

  createLink(userLink: LinkData) {
    const body = {
      userLink: userLink.userLink,
    }

    console.log(body);

    return this.http.post(environment.apiUrl + '/link', body);
  }
}
