import { Component, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LinkService } from './services/link.service';
import { LinkData } from './models/link.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  @ViewChild('f') form!: NgForm;
  newLink = '';

  constructor(private linkService: LinkService) { }

  createLink() {
    const linkData: LinkData = this.form.value;
    this.linkService.createLink(linkData).subscribe(result => {
      // @ts-ignore
      this.newLink = result.shortUrl;
    });
  }
}
